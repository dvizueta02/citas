<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas_has_terapias', function (Blueprint $table) {
            $table->integer('citas_id_citas')->index('fk_citas_has_terapias_citas_idx');
            $table->integer('terapias_id_terapias')->index('fk_citas_has_terapias_terapias1_idx');

            $table->primary(['citas_id_citas', 'terapias_id_terapias']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas_has_terapias');
    }
};
