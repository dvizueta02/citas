<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citas_has_terapias', function (Blueprint $table) {
            $table->foreign(['terapias_id_terapias'], 'fk_citas_has_terapias_terapias1')->references(['id_terapias'])->on('terapias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['citas_id_citas'], 'fk_citas_has_terapias_citas')->references(['id_citas'])->on('citas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citas_has_terapias', function (Blueprint $table) {
            $table->dropForeign('fk_citas_has_terapias_terapias1');
            $table->dropForeign('fk_citas_has_terapias_citas');
        });
    }
};
