<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horaraios_atención', function (Blueprint $table) {
            $table->integer('id_horarios_atención')->primary();
            $table->string('dia_horario_atención', 45)->nullable();
            $table->string('inicio_horario_atención', 45)->nullable();
            $table->string('fin_horario_atención', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horaraios_atención');
    }
};
