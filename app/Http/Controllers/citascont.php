<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\citas1;


class citascont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citasreport= citas1::all(); 
        return view ("citas.index", compact ("citasreport"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("citas.create") ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
            citas1::create($request->all());
            return redirect()->route('citasreport.index')->with('success','¡¡Su cita fue creada de forma exitosa!!');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(citas1 $citasreport)
    {
        return view('citas.edit', compact ("citasreport")); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $citasreport = citas1 :: find ($id);
            $citasreport -> update ($request -> all());
            return redirect () -> route ("citasreport.index") -> with ("success","¡¡Su  cita ha sido modificada con éxito!!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $citasreport = citas1 :: find ($id);
        $citasreport -> delete ();
        return redirect () -> route ("citasreport.index") -> with ("success"," ¡¡Fila de cita eliminada con éxito!!");

    }
}
