<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\diego;

class diego1 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
            $pacientereport = diego::all();
            return view ("Paciente.index", compact ("pacientereport"));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Paciente.create") ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        diego::create($request->all());
        return redirect()->route('pacientereport.index')->with('success','¡¡Su paciente fue creado de forma exitosa!!');
    }

 


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( diego $pacientereport )
    {
        return view('Paciente.edit', compact ("pacientereport")); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $pacientereport = diego :: find ($id);
            $pacientereport -> update ($request -> all());
            return redirect () -> route ("pacientereport.index") -> with ("success","¡¡Modificación  exitosa!!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pacientereport = diego :: find ($id);
        $pacientereport -> delete ();
        return redirect () -> route ("pacientereport.index") -> with ("success","¡¡Fila eliminada con éxito!!");

    }
}
