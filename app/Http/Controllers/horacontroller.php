<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\horamodel;                        /** vamos a utilizar el modelo aquí */



class horacontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()                      /** funcion metodo* 1*/
    {
        $horarios= horamodel::all(); 
        return view ("Horarios.index", compact ("horarios")); 
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        return view("Horarios.create") ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)   /* request_ estoy enviando datos que ingreso en el formulario */
    {
        
        horamodel::create($request->all());   /* en el modelo  vamo a crrear un registro en el request  */
        return redirect()->route('horariocontroller.index')->with('success','¡¡Su horario fue creado de forma exitosa!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id) 
    {
        $horarioo = horamodel :: find ($id);
        return view('Horarios.edit', compact ("horarioo")); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $horariocontroller = horamodel :: find ($id);
            $horariocontroller -> update ($request -> all());
            return redirect () -> route ("horariocontroller.index") -> with ("success","¡¡Su modificación ha sido realizada con éxito!!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horarioreport = horamodel :: find ($id);
        $horarioreport -> delete ();
        return redirect () -> route ("horariocontroller.index") -> with ("success"," ¡¡Fila eliminada con éxito!!");

    }
}
