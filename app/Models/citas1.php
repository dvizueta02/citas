<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class citas1 extends Model
{
    use HasFactory;
    protected $table = "citas";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id_citas",
        "fecha_cita",
        "hora_cita",
        
        
    ];
}
