<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class horamodel extends Model
{
    use HasFactory;
    protected $table = "horaraios_atención";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id",
        "dia_horario_atención",
        "inicio_horario_atención",
        "fin_horario_atención",
        
    ];
}
