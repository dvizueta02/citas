<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class diego extends Model
{
    use HasFactory;
    protected $table = "pacientes";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id_pacientes",
        "nombres_paciente",
        "telefono_paciente",
        "email_paciente",
        "ciudad_paciente",
    ];
}
