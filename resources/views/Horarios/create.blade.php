@extends('layouts.app')
@section('content')
    <div class="container">
        <br>
        <div align="center">
            <h2>CREAR HORARIO DE ATENCIÓN</h2>
        </div>
        <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('horariocontroller.index') }}"> Regresar</a>
        </div>
        <br>
        <br>

        <form action="{{ route('horariocontroller.store') }}" method="POST">          <!-- horario report _ dice _ lleveme al controlador-->  
            @csrf

            <div class="row">
               
                <div>
                    <div class="form-group">
                        <strong>Día Horario </strong>
                        <input type="date" name="dia_horario_atención" class="form-control"
                            placeholder="Ingrese el nombre">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong> Inicio Horario Atención</strong>
                        <input type="time" name="inicio_horario_atención" class="form-control"
                            placeholder="Ingrese fono">
                    </div>
                </div>
                
                <div>
                    <div class="form-group">
                        <strong>Fin Horario Atención</strong>
                        <input type="time" name="fin_horario_atención" class="form-control"
                            placeholder="Ingrese el color de la mesa deseada">
                    </div>
                </div>
                
                <br>
                <br>
                <br>
                <br>


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>   <!-- submit  es un btn _ para enviar , crear, modificar -->
                </div>
            </div>
        </form>
    </div>
@endsection