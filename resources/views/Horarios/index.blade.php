@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center"> <strong> HORARIO DE ATENCIÓN </strong>   </h1>
        <div>
            <a class="btn btn-success" href="{{ route('horariocontroller.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th> Day </th>
                <th>Hora_Inicio</th>
                <th>Hora_Fin</th>
                
                <th>Acciones</th>
            </tr>
            @foreach ($horarios as $horarioo)
                <tr>
                    <td>{{ $horarioo->id}}</td>
                    <td>{{ $horarioo->dia_horario_atención}}</td>
                    <td>{{ $horarioo->inicio_horario_atención }}</td>
                    <td>{{ $horarioo->fin_horario_atención }}</td>
                    <td>
                    
                      <!--  línea de codigo de iconos editar eliminar-->
                        
                      <div class="row">
                        <div class="col-sm-6"> 
                          
                        <a class="btn btn-primary" href="{{ route('horariocontroller.edit',$horarioo) }}">
                        <i class="fa-solid fa-pen-to-square"></i>  </a>

                    <form action="{{ route('horariocontroller.destroy',$horarioo->id) }}" method="POST">
                        @csrf
                    </div>
                   
                   <div class="col-sm-6">
                   

                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </div>
                    </form> 

                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection