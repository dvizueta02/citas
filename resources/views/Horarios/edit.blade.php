@extends('layouts.app')
@section('content')


   
    <div class="row">
    
        
        <div class="col-lg-12 margin-tb">
            

            
            <br />
            <div class="pull-left">
                <h2 align="center" > <strong>  EDITAR HORARIO  </strong>  </h2>
            </div>
            <br>

            <div class="container">

            <div class="pull-left">
                <a  class="btn btn-primary" href="{{ route('horariocontroller.index') }}"> Regresar</a>


            </div>
            <br>

        </div>
    </div>
    <form action="{{ route('horariocontroller.update', $horarioo->id) }}" method="POST">

        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Día:</strong>
                    <input type="varchar" name="dia_horario_atención" class="form-control" value="{{ $horarioo->dia_horario_atención }}">
                </div> 
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Inicio :</strong>
                    <input type="time" name="inicio_horario_atención" class="form-control" value="{{ $horarioo->inicio_horario_atención }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora fin:</strong>
                    <input type="time" name="fin_horario_atención" class="form-control" value="{{ $horarioo->fin_horario_atención }}">
                </div>
            </div>

            <br>
            <br>
            <br>
            <br>


            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection