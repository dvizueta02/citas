@extends('layouts.app')
@section('content')
    <div class="container">
        <br>
        <div align="center">
            <h2>CREAR NUEVA CITA</h2>
            <BR>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('citasreport.index') }}"> Regresar</a>
        </div>
        <br>
       

        <form action="{{ route('citasreport.store') }}" method="POST">
            @csrf

            <div class="row">
               
                <div>
                    <div class="form-group">
                        <strong>Fecha_Cita </strong>
                        <input type="Date" name="fecha_cita" class="form-control"
                            placeholder="Ingrese el nombre">
                    </div>
                </div>

                <br>
                <br>

                <div>
                    <div class="form-group">
                        <strong> Hora_Cita </strong>
                        <input type="Time" name="hora_cita" class="form-control"
                            placeholder="Ingrese fono">
                    </div>
                </div>

             

                <div>
             
                
                <br>
                <br>


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection