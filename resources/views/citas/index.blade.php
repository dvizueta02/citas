@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">CITAS </h1>
        <div>
            <a class="btn btn-success" href="{{ route('citasreport.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Fecha_Cita</th>
                <th>Hora_Cita</th>
                <th>Acciones</th>
            </tr>
            @foreach ($citasreport as $citass)
                <tr>
                    <td>{{ $citass->id}}</td>
                    <td>{{ $citass->fecha_cita }}</td>
                    <td>{{ $citass->hora_cita }}</td>

                    <td>
                        <!--  línea de codigo de iconos editar eliminar-->
                        
                     <div class="row">
                        <div class="col-sm-6"> 
                          
                        <a class="btn btn-primary" href="{{ route('citasreport.edit',$citass) }}">
                        <i class="fa-solid fa-pen-to-square"></i>  </a>

                    <form action="{{ route('citasreport.destroy',$citass->id) }}" method="POST">
                        @csrf
                    </div>
                   
                   <div class="col-sm-6">
                   

                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button> 
                    </div>
                    
                   
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
                   
                  