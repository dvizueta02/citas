@extends('layouts.app')
@section('content')


   
    <div class="row">
    
        
        <div class="col-lg-12 margin-tb">
            
            <br />
            <div class="pull-left">
                <h2 align="center" > <strong>  EDITAR CITA  </strong>  </h2>
            </div>
            <br>

            <div class="container">

            <div class="pull-left">
                <a class="btn btn-primary" href="{{ route('citasreport.index') }}"> Regresar</a>
            </div>
            <br>

        </div>
    </div>
    <form action="{{ route('citasreport.update',$citasreport->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Fecha_Cita:</strong>
                    <input type="varchar" name="fecha_cita" class="form-control" value="{{ $citasreport->fecha_cita }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora_Cita :</strong>
                    <input type="varchar" name="hora_cita" class="form-control" value="{{ $citasreport->hora_cita }}">
                </div>
            </div>

            <br>
            <br>
            <br>
            <br>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection