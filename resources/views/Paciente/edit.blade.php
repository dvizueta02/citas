@extends('layouts.app')
@section('content')


   
    <div class="row">
    
        
        <div class="col-lg-12 margin-tb">
            

            
            <br />
            <div class="pull-left">
                <h2 align="center" > <strong>  EDITAR DATO CLIENTE  </strong>  </h2>
            </div>
            <br>

            <div class="container">

            <div class="pull-left">
                <a class="btn btn-primary" href="{{ route('pacientereport.index') }}"> Regresar</a>
            </div>
            <br>

        </div>
    </div>
    <form action="{{ route('pacientereport.update',$pacientereport->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="varchar" name="nombres_paciente" class="form-control" value="{{ $pacientereport->nombres_paciente }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Telefono :</strong>
                    <input type="varchar" name="telefono_paciente" class="form-control" value="{{ $pacientereport->telefono_paciente }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Email :</strong>
                    <input type="varchar" name="email_paciente" class="form-control" value="{{ $pacientereport->email_paciente }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Ciudad:</strong>
                    <input type="varchar" name="ciudad_paciente" class="form-control" value="{{ $pacientereport->ciudad_paciente }}">
                </div>
            </div>

            <br>
            <br>
            <br>
            <br>


            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection