@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center"> <strong> DATOS DE PACIENTES</strong> </h1>
        <div>
            <a class="btn btn-success" href="{{ route('pacientereport.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Email</th>
                <th>Ciudad</th>
                <th>Acciones</th>
            </tr>
            @foreach ($pacientereport as $paciente)
                <tr>
                    <td>{{ $paciente->id}}</td>
                    <td>{{ $paciente->nombres_paciente }}</td>
                    <td>{{ $paciente->telefono_paciente }}</td>
                    <td>{{ $paciente->email_paciente }}</td>
                    <td>{{ $paciente->ciudad_paciente }}</td>
                   
                   
                    <td>
                        <!--  línea de codigo de iconos editar eliminar-->
                        
                     <div class="row">
                        <div class="col-sm-6"> 
                          
                        <a class="btn btn-primary" href="{{ route('pacientereport.edit',$paciente) }}">
                        <i class="fa-solid fa-pen-to-square"></i>  </a>

                    <form action="{{ route('pacientereport.destroy',$paciente->id) }}" method="POST">
                        @csrf
                    </div>
                   
                   <div class="col-sm-6">
                   

                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </div>
                    
                   
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection