@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2> <strong>  CREAR NUEVO PACIENTE </strong>  </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('pacientereport.index') }}"> Regresar</a>
        </div>
        <br>
        <br>

        <form action="{{ route('pacientereport.store') }}" method="POST">
            @csrf

            <div class="row">
               
                <div>
                    <div class="form-group">
                        <strong>Nombres </strong>
                        <input type="varchar" name="nombres_paciente" class="form-control"
                            placeholder="Ingrese sus nombres">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong> Teléfono</strong>
                        <input type="varchar" name="telefono_paciente" class="form-control"
                            placeholder="Ingrese telefono">
                    </div>
                </div>
                
                <div>
                    <div class="form-group">
                        <strong>Email</strong>
                        <input type="varchar" name="email_paciente" class="form-control"
                            placeholder="Ingrese email">
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Ciudad:</strong>
                        <input type="varchar" name="ciudad_paciente" class="form-control"
                            placeholder="Ingrese ciudad">
                    </div>
                </div>
              
                <br>
                <br>
                <br>
                <br>


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection